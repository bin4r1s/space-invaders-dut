﻿Voici le résumé des caractéristiques propres de mon Space Invaders :

Fonctionnalités ajoutées :
 - Des menus, incluant un écran de victoire, un écran de défaite et un classement (avec des animations)
 - Possibilité de rejouer (avec évolution du classement)
 - Système de points de vie pour le joueur (et perdre des vies augmente le score, qui doit être le plus bas possible)
 - Les conditions de défaite suivent celles du Space Invaders classique (plus de vie ou arrivée des Aliens en bas de l'écran)
 - Des barricades pour se protéger des tirs ennemis
 - Les aliens accélère au fur et à mesure que leur nombre diminue
 - Des ajustements pour le gameplay (cooldown entre les tirs, projectiles plus rapides, possibilité de détruire les projectiles de l'équipe adverse...)

Changements techniques apportés :
 - Une classe "Entite" a été ajoutée, et les classes Alien, Barricade, Projectile et Vaisseau héritent de celle-ci 
 - Certaines structures ont été ajoutées (comme les Directions, bien que très peu utilisées)
 - De nombreux affichages ont été déplacés vers le fichier Ascii.java pour faciliter la lecture du reste du code 
 - Aliens et Projectiles sont stockés dans une même liste d'entités (mais pas les barricades, bien qu'il aurait été possible de le faire)


Pour compiler, vous pouvez utiliser le Makefile (make).
Une règle existe pour compiler et lancer, utilisez juste "make run".

Sur ce, bonne correction !