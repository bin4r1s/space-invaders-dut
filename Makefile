OUT=bin
IN=src

FILES=\
$(OUT)/ChainePositionnee.class \
$(OUT)/EnsembleChaines.class \
$(OUT)/GestionJeu.class \
$(OUT)/Vaisseau.class \
$(OUT)/Projectile.class \
$(OUT)/Score.class \
$(OUT)/Alien.class \
$(OUT)/Entite.class \
$(OUT)/Ascii.class \
$(OUT)/Classement.class \
$(OUT)/Barricade.class \
$(OUT)/PositionChaine.class 

$(OUT)/Executable.class: bin/ $(FILES)
	javac $(IN)/Executable.java -d $(OUT)/ -sourcepath $(IN)/

$(OUT)/%.class: $(IN)/%.java
	javac $< -d $(OUT)/ -sourcepath $(IN)/

run: $(OUT)/Executable.class
	java -cp $(OUT) Executable

bin/:
	mkdir bin/

clean:
	rm -rf $(OUT)/*.class