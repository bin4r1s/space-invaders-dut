public class Score
{
    private int score;
    private String nomJoueur;

    public Score(String nomDuJoueur)
    {
        this.score = 0;
        this.nomJoueur = nomDuJoueur;
    }

    public void ajouterPoints(int points)
    {
        this.score += points;
    }

    public int getScore()
    {
        return this.score;
    }

    public String getNomJoueur()
    {
        return this.nomJoueur;
    }

    public EnsembleChaines getEnsembleChaines()
    {
        EnsembleChaines affichageScore = new EnsembleChaines();

        affichageScore.ajouteChaine(1, 58, "Score : " + this.score);

        return affichageScore;
    }
}