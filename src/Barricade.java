public class Barricade extends Entite 
{
    public Barricade(double posX, double posY)
    {
        super(posX, posY);
        this.affichage.union(Ascii.toEnsembleChaines((int)posX, (int)posY, Ascii.WALL));
    }

    public void hit(double x, double y)
    {
        this.affichage.supprimer((int)x, (int)y);
    }

    public void evolue() {}
}