import java.util.ArrayList;

public class EnsembleChaines 
{
    ArrayList<ChainePositionnee> chaines;
    
    public EnsembleChaines()
    {
        chaines = new ArrayList<ChainePositionnee>(); 
    }

    public void ajouteChaine(int x, int y, String c)
    {
        chaines.add(new ChainePositionnee(x, y, c));
    }

    public void union(EnsembleChaines e)
    {
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }

    public boolean contient(int posX, int posY)
    {
        for(ChainePositionnee c : chaines)
        {
            if(c.contient(posX, posY))
                return true;
        }
        return false;
    }

    /**
     * Place un espace aux coordonnées fournies (si une chaîne positionnée est présente à cet endroit)
     * @param x
     * @param y
     */
    public void supprimer(int x, int y)
    {
        for(ChainePositionnee c : this.chaines)
        {
            if(c.contient(x, y))
            {
                c.remplacer(x, ' ');
            }
        }
    }

    public ArrayList<PositionChaine> casesNonVides()
    {
        ArrayList<PositionChaine> resultat = new ArrayList<>();

        for(ChainePositionnee c : this.chaines)
        {
            for(int i = 0; i < c.c.length(); i++)
            {
                if(c.contient(c.x + i, c.y))
                {
                    resultat.add(new PositionChaine(c.x + i, c.y));
                }
            }
        }

        return resultat;
    }

    public void vider()
    { 
        chaines.clear();
    }
}
