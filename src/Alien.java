public class Alien extends Entite
{
    private int ticksExisted;
    private double vitesse;

    public Alien(double posX, double posY)
    {
        super(posX, posY);
        this.ticksExisted = 0;
        this.vitesse = 0.2D;
    }

    public EnsembleChaines getEnsembleChaines()
    {
        this.affichage.vider();

        if(!this.isDead) 
        {
            if(this.ticksExisted % 30 < 15)
            {
                this.affichage.union(Ascii.toEnsembleChaines((int)this.posX, (int)this.posY, Ascii.ALIEN_MOVING_0));
            }
            else 
            {
                this.affichage.union(Ascii.toEnsembleChaines((int)this.posX, (int)this.posY, Ascii.ALIEN_MOVING_1));
            }
        }
        else 
        {
            this.affichage.union(Ascii.toEnsembleChaines((int)this.posX, (int)this.posY, Ascii.ALIEN_HIT));
        }
        
        return this.affichage;
    }

    public void evolue()
    {
        if(!this.isDead)
        {
            //this.posX += (this.ticksExisted % 200 < 100) ? 0.2 : -0.2;

            this.posX += this.vitesse;

            /*if(this.ticksExisted % 100 == 0)
               this.posY -= 1;*/
        
            this.ticksExisted++;
        }
        else 
        {
            if(this.ticksExisted > 0)
                this.ticksExisted = 0;
            
            this.ticksExisted--;
        }
    }

    public boolean doitTourner(int limiteX)
    {
        return this.vitesse > 0 ? (this.posX + this.vitesse + getLargeur() > limiteX) : (this.posX - this.vitesse < 1);
    }

    public void tourner()
    {
        this.posY -= 1;
        this.vitesse = -this.vitesse;
    }

    public void allerPlusVite()
    {
        this.vitesse *= 1.1D;
    }

    public int positionCanon()
    {
        return (int)(this.posX + (getLargeur() / 2));
    }

    public boolean shallBeRemoved()
    {
        return (this.isDead && this.ticksExisted < -20);
    }

    /* Les fonctions suivantes permettent de connaître les dimensions d'un alien */
    public static int getLargeur()
    {
        return 11;
    }

    public static int getHauteur()
    {
        return 5;
    }
}