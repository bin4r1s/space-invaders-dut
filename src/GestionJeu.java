import java.util.ArrayList;
import java.util.Random;

public class GestionJeu
{
    private Vaisseau vaisseau;
    private ArrayList<Entite> entitesNonJouables;
    private ArrayList<Entite> objetsNeutres; // touchés par toutes les équipes, utilisé pour les barricades

    private EnsembleChaines affichage;
    private Score score;
    private Random random;

    private Menus menu;
    private Classement classement;

    private int animationTicks;
    private byte shootCooldown;

    public GestionJeu()
    {
        this.vaisseau = new Vaisseau(43);
        this.entitesNonJouables = new ArrayList<>();
        this.objetsNeutres = new ArrayList<>();
        this.affichage = new EnsembleChaines();
        this.score = new Score(System.getProperty("user.name"));
        this.random = new Random();
        this.menu = Menus.PARTIE; // TODO à changer quand menu principal
        this.classement = new Classement();
        this.animationTicks = 0;
        this.shootCooldown = 0;

        this.placerAliens();
        this.placerBarricades();
    }

    public void placerAliens()
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                int posX = 2 + (j * (Alien.getLargeur() + 5) ); // on place un alien tous les (largeur d'un alien + 5) (en laissant une marge de 2 par rapport au bord)
                int posY = this.getHauteur() - 4 - ( i * (Alien.getHauteur() + 2) ); // on place les aliens à partir de la 4e case en partant du haut et espacés de 2 cases

                this.entitesNonJouables.add(new Alien(posX, posY));
            }
        }
    }

    public void placerBarricades()
    {
        for(int i = 0; i < 3; i++)
        {
            this.objetsNeutres.add(new Barricade(11 + (i * 32), 10));
        }
    }

    public int getHauteur()
    {
        return 60;
    }

    public int getLargeur()
    {
        return 100;
    }

    public void toucheGauche()
    {
        // si au prochain mouvement on sort de l'écran (x < 0), on ne bouge pas
        if(this.vaisseau.getPosX() >= this.vaisseau.getVitesse()) 
        {
            this.vaisseau.deplacer(Direction.GAUCHE);
        }
    }

    public void toucheDroite()
    {
        // si au prochain mouvement on sort de l'écran (x > this.getLargeur()), on ne bouge pas
        if(this.vaisseau.getPosX() + this.vaisseau.getLargeur() <= this.getLargeur() - this.vaisseau.getVitesse())
        {
            this.vaisseau.deplacer(Direction.DROITE);
        }
    }

    public void toucheEspace()
    {
        if(this.menu == Menus.GAME_OVER || this.menu == Menus.CLASSEMENT)
        {
            this.reset(); // on redémarre une partie
        }
        else if(this.menu == Menus.VICTOIRE)
        {
            this.menu = Menus.CLASSEMENT;
            this.classement.ajouterScore(this.score);
        }
        else if(this.menu == Menus.PARTIE) 
        {
            // on vérifie qu'il n'y a pas déjà un projectile là où le nouveau projectile sera placé (on évite le spam abusif)
            if(!this.affichage.contient(this.vaisseau.positionCanon(), 4) && this.shootCooldown == 0)
            {
                this.entitesNonJouables.add(new Projectile(this.vaisseau.positionCanon(), 4, 0.8D, true));
                this.shootCooldown = 10;
            }
        }
    }

    public EnsembleChaines getChaines()
    {
        this.affichage.vider();
        
        if(this.menu == Menus.PARTIE)
        {
            this.affichage.union(this.score.getEnsembleChaines());
            this.affichage.union(this.vaisseau.getEnsembleChaines());

            for(Entite entite : this.entitesNonJouables)
            {
                this.affichage.union(entite.getEnsembleChaines());
            }

            for(Entite entite : this.objetsNeutres)
            {
                this.affichage.union(entite.getEnsembleChaines());
            }
        }
        else if(this.menu == Menus.VICTOIRE)
        {
            this.affichage.union(Ascii.texteCentre(38, this.getLargeur(), Ascii.WIN_MSG));
        }
        else if(this.menu == Menus.GAME_OVER)
        {
            this.affichage.union(Ascii.texteCentre(40, this.getLargeur(), Ascii.GAMEOVER_MSG));
        }
        else if(this.menu == Menus.CLASSEMENT)
        {
            this.affichage.union(this.classement.getEnsembleChaines());
            
        }

        if(this.menu == Menus.CLASSEMENT || this.menu == Menus.GAME_OVER || this.menu == Menus.VICTOIRE)
        {
            if(this.animationTicks % 60 < 30)
                this.affichage.union(Ascii.texteCentre(8, this.getLargeur(), Ascii.PRESS_SPACE));
        }

        this.animationTicks++;

        return this.affichage;
    }

    public void jouerUnTour()
    {
        if(this.menu == Menus.PARTIE)
        {
            int alienCount = 0;
            int removedAliens = 0;
            boolean tournerAliens = false;

            // on crée une liste intermédiaire pour éviter de modifier la liste des entités non jouables en même temps qu'on la parcourt
            ArrayList<Entite> nouvellesEntites = new ArrayList<>();
            
            for(Entite entite : this.entitesNonJouables)
            {
                entite.evolue();

                if(entite instanceof Projectile)
                {
                    Projectile proj = (Projectile)entite;

                    // si le projectile sort de l'écran
                    if(proj.getPosY() > this.getHauteur() || proj.getPosY() < 0)
                    {
                        proj.setDead();
                        continue;
                    }

                    for(Entite entite_collisionnable : this.objetsNeutres)
                    {
                        if(proj.isDead()) continue;

                        if(entite_collisionnable instanceof Barricade)
                        {
                            if(entite_collisionnable.contient(proj.getPosX(), proj.getPosY()))
                            {
                                ((Barricade)entite_collisionnable).hit(proj.getPosX(), proj.getPosY());
                                proj.setDead();
                            }
                        }
                    }

                    if(proj.isOwnedByPlayer())
                    {
                        for(Entite entite_collisionnable : this.entitesNonJouables)
                        {
                            if(proj.isDead()) continue; // si le projectile a déjà heurté quelque chose, on passe
                            
                            // un projectile ne peut pas entrer en collision avec lui-même
                            if(entite_collisionnable == entite)
                                continue;
                            
                            if(entite_collisionnable instanceof Projectile)
                            {
                                // on évite que les projectiles de chaque équipe rentrent en collision avec d'autres projectiles de l'équipe
                                if(((Projectile)entite_collisionnable).isOwnedByPlayer() == proj.isOwnedByPlayer())
                                {
                                    continue;
                                }
                            }
                            
                            // si un élément autre que le joueur (Alien, Projectile ennemi) entre en collision avec le projectile, il meurt
                            if(entite_collisionnable.contient(proj.getPosX(), proj.getPosY()))
                            {
                                entite_collisionnable.setDead();
                                proj.setDead();

                                if(entite_collisionnable instanceof Alien) removedAliens++;
                            }
                        }
                    }
                    else 
                    {
                        // si le joueur est touché par un projectile étranger
                        if(this.vaisseau.contient((int)proj.getPosX(), (int)proj.getPosY()))
                        {
                            this.vaisseau.hit();
                            this.score.ajouterPoints(1000);
                            proj.setDead();
                        }
                    }
                }
                else if(entite instanceof Alien && !entite.shallBeRemoved())
                {
                    // les aliens envoient aléatoirement des projectiles
                    if(this.random.nextInt(1000) == 0)
                    {
                        nouvellesEntites.add(new Projectile(((Alien)entite).positionCanon(), ((Alien)entite).getPosY() - 1, -1.0D, false));
                    }

                    // on fait le test de collision seulement si l'alien est assez proche (pour économiser de la ressource)
                    if(this.vaisseau.distanceTo(entite) < Alien.getLargeur())
                    {
                        // si un alien entre en collision avec le joueur, il perd la partie
                        if(this.vaisseau.isColliding(entite))
                        {
                            this.vaisseau.kill();
                        }
                    }

                    // on gère les collision avec les objets qui n'appartiennent à aucune équipe
                    for(Entite entiteNeutre : this.objetsNeutres)
                    {
                        // si un alien rentre en collision avec une barricade
                        if(entiteNeutre.distanceTo(entite) < Alien.getLargeur() && entiteNeutre instanceof Barricade)
                        {
                            // alors on supprime toutes les cases de la barricade sur lesquelles l'alien passe
                            for(PositionChaine pos : entite.casesNonVides())
                            {
                                if(entiteNeutre.contient(pos.getX(), pos.getY()))
                                {
                                    ((Barricade)entiteNeutre).hit(pos.getX(), pos.getY());
                                }
                            }
                        }
                    }

                    if(entite.posY - Alien.getHauteur() < 0) // ça prend à x = 3 ??? TODO
                        this.vaisseau.kill(); // la partie est finie, les aliens ont atteint le bas de l'écran
                    
                    if(((Alien)entite).doitTourner(this.getLargeur())) tournerAliens = true;

                    
                    alienCount++;
                }
            }

            // on rajoute les nouvelles entités à la liste des entités non jouables (projectiles des aliens notamment)
            this.entitesNonJouables.addAll(nouvellesEntites);

            // on élimine les entités qui sont mortes
            for(int i = 0; i < this.entitesNonJouables.size(); i++)
            {
                if(this.entitesNonJouables.get(i).shallBeRemoved())
                {
                    this.entitesNonJouables.remove(i);
                }
            }

            // si un alien a atteind le bord de l'écran
            if(tournerAliens)
            {
                for(Entite entite : this.entitesNonJouables)
                {
                    if(entite instanceof Alien)
                    {
                        ((Alien)entite).tourner();
                    }
                }
            }

            for(int i = 0; i < removedAliens; i++)
            {
                for(Entite entite : this.entitesNonJouables)
                {
                    if(entite instanceof Alien)
                    {
                        ((Alien)entite).allerPlusVite();
                    }
                }
            }

            if(alienCount == 0) // la partie est finie et le joueur gagne
            {
                this.menu = Menus.VICTOIRE;
            }
            else if(this.vaisseau.isDead()) // la partie est finie (et le joueur a perdu)
            {
                this.menu = Menus.GAME_OVER;
            }
            else 
            {
                this.score.ajouterPoints(1);
            }
        }

        if(this.shootCooldown > 0)
            this.shootCooldown--;
    }

    public void reset()
    {
        this.vaisseau = new Vaisseau(43);

        this.entitesNonJouables.clear();
        this.objetsNeutres.clear();
        this.placerAliens();
        this.placerBarricades();
        
        this.score = new Score(System.getProperty("user.name"));
        this.menu = Menus.PARTIE;
        this.animationTicks = 0;
    }

    public static enum Menus 
    {
        MENU_PRINCIPAL,
        PARTIE,
        GAME_OVER,
        VICTOIRE,
        CLASSEMENT
    }
}