public class Projectile  extends Entite
{
    private double vitesse;
    private boolean ownedByPlayer;

    public Projectile(double posX, double posY, double vitesse, boolean ownedByPlayer)
    {
        super(posX, posY);
        this.vitesse = vitesse;
        this.ownedByPlayer = ownedByPlayer;
    }

    public EnsembleChaines getEnsembleChaines()
    {
        this.affichage.vider();

        if(this.vitesse > 0)
        {
            this.affichage.ajouteChaine((int)this.posX, (int)this.posY, "^");
        }
        else 
        {
            this.affichage.ajouteChaine((int)this.posX, (int)this.posY, "o");
        }

        return super.getEnsembleChaines();
    }

    public void evolue()
    {
        this.posY += this.vitesse;
    }

    public boolean isOwnedByPlayer()
    {
        return this.ownedByPlayer;
    }
}