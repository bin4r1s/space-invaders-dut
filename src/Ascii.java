public class Ascii 
{
    public static final String[] PLAYER = new String[] {
        "      ▄",
        "     ███",
        "▄███████████▄",
        "█████████████"
    };

    public static final String[] ALIEN_MOVING_0 = new String[] {
        "  ▀▄  ▄▀",
        " ▄█▀███▀█▄",
        "█▀███████▀█",
        "█ █▀▀▀▀▀█ █",
        "   ▀▀ ▀▀"
    };

    public static final String[] ALIEN_MOVING_1 = new String[] {
        "   ▀▄  ▄▀",
        " ▄█▀███▀█▄",
        "█▀███████▀█",
        "█ █▀▀▀▀▀█ █",
        " ▀▀     ▀▀"
    };

    public static final String[] ALIEN_HIT = new String[] {
        "▄ ▀▄   ▄▀ ▄",
        "█▄███████▄█",
        "███▄███▄███",
        "▀█████████▀",
        " ▄▀     ▀▄"
    };

    public static final String[] WALL = new String[] {
        " ▄▄▄▄▄▄▄▄▄▄▄ ",
        "█████████████",
        "▀▀▀▀▀▀▀▀▀▀▀▀▀"
    };

    public static final String[] WIN_MSG = new String[] {
        "██╗   ██╗ ██████╗ ██╗   ██╗    ██╗    ██╗██╗███╗   ██╗",
        "╚██╗ ██╔╝██╔═══██╗██║   ██║    ██║    ██║██║████╗  ██║",
        " ╚████╔╝ ██║   ██║██║   ██║    ██║ █╗ ██║██║██╔██╗ ██║",
        "  ╚██╔╝  ██║   ██║██║   ██║    ██║███╗██║██║██║╚██╗██║",
        "   ██║   ╚██████╔╝╚██████╔╝    ╚███╔███╔╝██║██║ ╚████║",
        "   ╚═╝    ╚═════╝  ╚═════╝      ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝",
        "", "", "", "", "",
        "Félicitations, vous avez gagné !"
    };

    public static final String[] GAMEOVER_MSG = new String[] {
        " ██████╗  █████╗ ███╗   ███╗███████╗     ██████╗ ██╗   ██╗███████╗██████╗ ",
        "██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔═══██╗██║   ██║██╔════╝██╔══██╗",
        "██║  ███╗███████║██╔████╔██║█████╗      ██║   ██║██║   ██║█████╗  ██████╔╝",
        "██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██║   ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗",
        "╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ╚██████╔╝ ╚████╔╝ ███████╗██║  ██║",
        " ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝     ╚═════╝   ╚═══╝  ╚══════╝╚═╝  ╚═╝",
        "",
        "",
        "",
        "",
        "",
        "Vous avez perdu !",
        "",
        "Mais ne vous découragez pas, vous pouvez gagner !"
    };

    public static final String[] PRESS_SPACE = new String[] {
        "Appuyez sur ESPACE pour continuer"
    };
    
    public static EnsembleChaines toEnsembleChaines(int x, int y, String[] asciiArt)
    {
        EnsembleChaines resultat = new EnsembleChaines();

        for(int i = 0; i < asciiArt.length; i++)
        {
            resultat.ajouteChaine(x, y - i, asciiArt[i]);
        }

        return resultat;
    }

    public static EnsembleChaines texteCentre(int posY, int largeurEcran, String[] texte)
    {
        EnsembleChaines resultat = new EnsembleChaines();

        for(int i = 0; i < texte.length; i++)
        {
            resultat.ajouteChaine((int)(largeurEcran / 2 - (texte[i].length() / 2)), posY - i, texte[i]);
        }
        
        return resultat;
    }
}