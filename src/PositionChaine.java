/**
 * Cette classe est utilisée pour représenter une position dans une chaine positionnée / un ensemble de chaines
 * Elle n'est utilisée que par la méthode casesNonVides() de EnsembleChaines (et toutes les méthodes utilisant cette dernière)
 */
public class PositionChaine
{
    private int x, y;

    public PositionChaine(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }
}