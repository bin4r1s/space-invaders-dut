import java.util.ArrayList;

public abstract class Entite 
{
    protected double posX;
    protected double posY;
    protected boolean isDead;

    protected EnsembleChaines affichage;

    public Entite(double posX, double posY)
    {
        this.posX = posX;
        this.posY = posY;
        this.isDead = false;
        this.affichage = new EnsembleChaines();
    }

    public double getPosX()
    {
        return this.posX;
    }

    public double getPosY()
    {
        return this.posY;
    }

    public boolean isDead()
    {
        return this.isDead;
    }

    public void setDead()
    {
        this.isDead = true;
    }

    public EnsembleChaines getEnsembleChaines()
    {
        return this.affichage;
    }

    public ArrayList<PositionChaine> casesNonVides()
    {
        return this.affichage.casesNonVides();
    }

    public boolean contient(double x, double y)
    {
        return this.affichage.contient((int)x, (int)y);
    }

    public double distanceTo(Entite entite)
    {
        double deltaX = this.getPosX() - entite.getPosX();
        double deltaY = this.getPosY() - entite.getPosY();
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public boolean shallBeRemoved()
    {
        return this.isDead;
    }

    public abstract void evolue();
}