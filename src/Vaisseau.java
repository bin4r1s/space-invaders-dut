public class Vaisseau extends Entite
{
    private double vitesse;
    private int lives;

    public Vaisseau(double posX)
    {
        super(posX, 0);
        this.lives = 3;
        this.vitesse = 1.0D;
    }

    public void deplacer(Direction direction)
    {
        this.posX += this.vitesse * (direction == Direction.DROITE ? 1 : -1);
    }

    public int positionCanon()
    {
        return (int)this.posX + 6;
    }

    public double getVitesse()
    {
        return this.vitesse;
    }

    public void setVitesse(double vitesse)
    {
        this.vitesse = vitesse;
    }

    public void hit()
    {
        this.lives--;
    }

    public void kill()
    {
        this.lives = 0;
    }

    public int getLivesLeft()
    {
        return this.lives;
    }

    @Override
    public boolean isDead()
    {
        return this.getLivesLeft() <= 0;
    }

    public EnsembleChaines getEnsembleChaines()
    {
        this.affichage.vider();

        this.affichage.union(Ascii.toEnsembleChaines((int)this.posX, 3, Ascii.PLAYER));
        this.affichage.ajouteChaine(90, 58, "Vies : " + this.lives);

        return super.getEnsembleChaines();
    }

    public int getLargeur()
    {
        return 13;
    }

    public boolean isColliding(Entite entite)
    {
        for(ChainePositionnee cp : this.affichage.chaines)
        {
            for(int i = 0; i < cp.c.length(); i++)
            {
                if(!((Character)cp.c.charAt(i)).equals(' '))
                {
                    if(entite.contient(cp.x + i, cp.y))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // ne sert à rien, mais nécessaire car hérite de la classe abstraite Entite
    public void evolue() { }
}