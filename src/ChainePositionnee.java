public class ChainePositionnee
{
    int x, y;
    String c;
    
    public ChainePositionnee(int a,int b, String d) 
    {
        x = a; 
        y = b; 
        c = d;
    }

    public boolean contient(int posX, int posY)
    {
        if(this.x <= posX && posX < (this.x + this.c.length()) && this.y == posY)
            if(!((Character)this.c.charAt(posX - this.x)).equals(' '))
                return true;

        return false;
    }

    public void remplacer(int posX, char aInserer)
    {
        if(posX < this.x + this.c.length())
            this.c = (this.c.substring(0, posX - this.x)) + aInserer + (this.c.substring(posX + 1 - this.x, this.c.length()));
    }
}
