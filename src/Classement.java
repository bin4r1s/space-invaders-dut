import java.util.ArrayList;

public class Classement 
{
    private ArrayList<Score> classement;
    private int dernierRangAjoute;

    private int animationTicks;

    public Classement()
    {
        this.classement = new ArrayList<>();
        this.dernierRangAjoute = 0;
        this.animationTicks = 0;
    }

    public void ajouterScore(Score score)
    {
        ArrayList<Score> nouveauClassement = new ArrayList<>();
        int rank = 0;
        /*boolean stored = false;

        if(this.classement.size() > 0)
        {
            for(; rank < this.classement.size() && rank < 5; rank++)
            {
                Score currentScore = this.classement.get(rank);
    
                if(currentScore.getScore() < score.getScore())
                {
                    nouveauClassement.add(currentScore);
                }
                else if(!stored)
                {
                    nouveauClassement.add(score); // on insère le nouveau score
                    nouveauClassement.add(currentScore); // puis on rajoute celui présent initialement après (= insertion)
                    this.dernierRangAjoute = rank;
                    stored = true;
                }
                else 
                {
                    nouveauClassement.add(currentScore);
                }

                if(rank == this.classement.size() - 1 && !stored)
                    nouveauClassement.add(score);
    
                rank++;
            }
        }
        else 
        {
            nouveauClassement.add(score);
            this.dernierRangAjoute = rank;
        }
        */

        if(this.classement.size() > 0)
        {
            while(rank < this.classement.size() && this.classement.get(rank).getScore() < score.getScore())
            {
                nouveauClassement.add(this.classement.get(rank));
                rank++;
            }
        }
        
        nouveauClassement.add(score);
        this.dernierRangAjoute = rank;

        while(rank < this.classement.size())
        {
            nouveauClassement.add(this.classement.get(rank));
            rank++;
        }

        this.classement.clear();
        this.classement.addAll(nouveauClassement.subList(0, (nouveauClassement.size() >= 5 ? 5 : nouveauClassement.size())));
        this.animationTicks = 0; // réinitialisé nulle part ailleurs, sert pour l'affichage en "cascade"
    }

    public EnsembleChaines getEnsembleChaines()
    {
        EnsembleChaines affichage = new EnsembleChaines();

        affichage.ajouteChaine(42, 40, "MEILLEURS SCORES");
        affichage.ajouteChaine(33, 37, "Rang");
        affichage.ajouteChaine(41, 37, "Nom");
        affichage.ajouteChaine(61, 37, "Score");


        for(int i = 0; i < this.classement.size(); i++)
        {
            if(this.animationTicks > (i * 20))
            {
                if(i != this.dernierRangAjoute ||
                   this.animationTicks % 40 < 20 || // pour faire clignoter le dernier score ajouté (si présent)
                   this.animationTicks < (this.classement.size() * 20) ) // pour qu'il ne clignote qu'après que tous les scores aient été ajoutés
                {
                    Score score = this.classement.get(i);
                    int y = 35 - (i * 2);
        
                    affichage.ajouteChaine(33, y, (i + 1) + ".");
                    affichage.ajouteChaine(41, y, score.getNomJoueur());
                    affichage.ajouteChaine(61, y, score.getScore() + "");
                }
            }
            
        }

        this.animationTicks++;

        return affichage;
    }
}